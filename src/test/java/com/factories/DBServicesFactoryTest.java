package com.factories;

import com.services.dbServices.IDBService;
import com.services.dbServices.impl.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import static org.junit.jupiter.api.Assertions.*;

class DBServicesFactoryTest {

    private final JsonDBService json = Mockito.mock(JsonDBService.class);
    private final BinaryDBService binary = Mockito.mock(BinaryDBService.class);
    private final XmlDBService xml = Mockito.mock(XmlDBService.class);
    private final YamlDBService yaml = Mockito.mock(YamlDBService.class);
    private final CsvDBService csv = Mockito.mock(CsvDBService.class);
    private final PostgresqlDBService postgresql = Mockito.mock(PostgresqlDBService.class);
    private final MySqlDBService mySql = Mockito.mock(MySqlDBService.class);
    private final RedisDBService redis = Mockito.mock(RedisDBService.class);
    private final MongoDBService mongo = Mockito.mock(MongoDBService.class);
    private final H2DBService h2 = Mockito.mock(H2DBService.class);
    private final CassandraDBService cassandra = Mockito.mock(CassandraDBService.class);
    private final GraphDBService graph = Mockito.mock(GraphDBService.class);
    private final IDBService[] services = {json, binary, xml, yaml, csv, postgresql, mySql, redis, mongo, h2,
            cassandra, graph};
    private final DBServicesFactory cut = new DBServicesFactory(services);

    static Arguments[] getDBServiceTestArgs() {
        return new Arguments[]{
                Arguments.arguments(JsonDBService.class, 0),
                Arguments.arguments(BinaryDBService.class, 1),
                Arguments.arguments(XmlDBService.class, 2),
                Arguments.arguments(YamlDBService.class, 3),
                Arguments.arguments(CsvDBService.class, 4),
                Arguments.arguments(PostgresqlDBService.class, 5),
                Arguments.arguments(MySqlDBService.class, 6),
                Arguments.arguments(RedisDBService.class, 7),
                Arguments.arguments(MongoDBService.class, 8),
                Arguments.arguments(H2DBService.class, 9),
                Arguments.arguments(CassandraDBService.class, 10),
                Arguments.arguments(GraphDBService.class, 11)
        };
    }

    @ParameterizedTest
    @MethodSource("getDBServiceTestArgs")
    void getDBServiceTest(Class expected, int id) {

        IDBService actual = cut.getDBService(id);

        assertSame(actual.getClass(), expected);
    }

    @Test
    void getDBServiceExceptionTest() {
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> cut.getDBService(-1));
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> cut.getDBService(12));
    }
}