package com.services.dbServices.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.models.Person;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


class YamlDBServiceTest {
    private static final Person p1 = new Person(1, "F1", "L1", 20, "C1");
    private static final Person p2 = new Person(2, "F1", "L1", 20, "C1");
    private static final Person p3 = new Person(3, "F1", "L1", 20, "C1");
    private static final Person[] personArr = {p2, p3};
    private final File file = Mockito.mock(File.class);
    private final ObjectMapper objectMapper = Mockito.mock(ObjectMapper.class);


    private final YamlDBService cut = new YamlDBService(file, objectMapper);

    static List<Arguments> createTestArgs() {
        return List.of(
                Arguments.arguments(1L, new ArrayList<>(Arrays.asList(p2, p3, p1))),
                Arguments.arguments(0L, new ArrayList<>(Arrays.asList(p1)))
        );
    }

    @ParameterizedTest
    @MethodSource("createTestArgs")
    void createTest(long lengthOfFile, ArrayList<Person> buffer) {
        try {
            Mockito.when(file.length()).thenReturn(lengthOfFile);
            Mockito.when(objectMapper.readValue(file, Person[].class)).thenReturn(personArr);

            cut.create(p1);

            Mockito.verify(objectMapper, Mockito.times(1)).writeValue(file, buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void read() {
        try {
            Mockito.when(file.length()).thenReturn(1L);
            Mockito.when(objectMapper.readValue(file, Person[].class)).thenReturn(personArr);

            cut.read();

            Mockito.verify(objectMapper, Mockito.times(1)).readValue(file, Person[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void updateTest() {
        try {
            Mockito.when(file.length()).thenReturn(1L);
            Mockito.when(objectMapper.readValue(file, Person[].class)).thenReturn(personArr);
            Person updated = new Person(2, "upd", "upd", 20, "upd");
            ArrayList<Person> updList = new ArrayList<>(Arrays.asList(updated, p3));

            cut.update(updated);

            Mockito.verify(objectMapper, Mockito.times(1)).writeValue(file, updList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void deleteTest() {
        try {
            Mockito.when(file.length()).thenReturn(1L);
            Mockito.when(objectMapper.readValue(file, Person[].class)).thenReturn(personArr);

            cut.delete(p3);

            Mockito.verify(objectMapper, Mockito.times(1)).writeValue(file, new ArrayList<>(Arrays.asList(p2)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}