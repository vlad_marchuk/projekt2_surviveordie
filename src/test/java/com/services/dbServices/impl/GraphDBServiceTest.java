package com.services.dbServices.impl;

import com.models.Person;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import java.sql.*;
import java.util.List;

class GraphDBServiceTest {

    private static final String DB_URL = "jdbc:neo4j:bolt://localhost:7687";
    private static final String USER = "neo4j";
    private static final String PASSWORD = "123qwe";
    private static final String REQUEST_CREATE = "CREATE (person:Person {id: ?, fname: ?, lname: ?, age: ?, city: ?}) RETURN person";
    private static final String REQUEST_READ = "MATCH (person:Person) RETURN person.id, person.fname, person.lname, person.age, person.city";
    private static final String REQUEST_UPDATE = "MATCH (person:Person) WHERE person.id=? SET person.fname=? SET person.lname=? " +
            "SET person.age=? SET person.city=? RETURN person";
    private static final String REQUEST_DELETE = "MATCH (person:Person) WHERE person.id=? DELETE person";

    private final Connection connection = Mockito.mock(Connection.class);
    private final PreparedStatement preparedStatement = Mockito.mock(PreparedStatement.class);
    private final Statement statement = Mockito.mock(Statement.class);
    private final ResultSet resultSet = Mockito.mock(ResultSet.class);
    private final Person personTest = new Person(123, "F1", "L1", 20, "C1");

    private final GraphDBService cut = new GraphDBService();

    @Test
    void createTest() {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager.getConnection(DB_URL, USER, PASSWORD)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(REQUEST_CREATE)).thenReturn(preparedStatement);

            cut.create(personTest);

            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(1, personTest.id);
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(2, personTest.fname);
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(3, personTest.lname);
            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(4, personTest.age);
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(5, personTest.city);
            Mockito.verify(preparedStatement, Mockito.times(1)).executeUpdate();
            Mockito.verify(preparedStatement, Mockito.times(1)).close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static List<Arguments> readTestArgs() {
        return List.of(
                Arguments.arguments(true, 1, 2, 1, 1, 1, 1, 1),
                Arguments.arguments(false, 1, 1, 0, 0, 0, 0, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("readTestArgs")
    void readTest(boolean next, int executeQueryTimes, int nextTimes, int idTimes, int fNameTimes, int lNameTimes,
                  int ageTimes, int cityTimes) {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager.getConnection(DB_URL, USER, PASSWORD)).thenReturn(connection);
            Mockito.when(connection.createStatement()).thenReturn(statement);
            Mockito.when(statement.executeQuery(REQUEST_READ)).thenReturn(resultSet);
            Mockito.when(resultSet.next()).thenReturn(next).thenReturn(false);

            cut.read();

            Mockito.verify(statement, Mockito.times(executeQueryTimes)).executeQuery(REQUEST_READ);
            Mockito.verify(resultSet, Mockito.times(nextTimes)).next();
            Mockito.verify(resultSet, Mockito.times(idTimes)).getInt("person.id");
            Mockito.verify(resultSet, Mockito.times(fNameTimes)).getString("person.fname");
            Mockito.verify(resultSet, Mockito.times(lNameTimes)).getString("person.lname");
            Mockito.verify(resultSet, Mockito.times(ageTimes)).getInt("person.age");
            Mockito.verify(resultSet, Mockito.times(cityTimes)).getString("person.city");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void updateTest() {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager.getConnection(DB_URL, USER, PASSWORD)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(REQUEST_UPDATE)).thenReturn(preparedStatement);

            cut.update(personTest);

            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(1, personTest.id);
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(2, personTest.fname);
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(3, personTest.lname);
            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(4, personTest.age);
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(5, personTest.city);
            Mockito.verify(preparedStatement, Mockito.times(1)).executeUpdate();
            Mockito.verify(preparedStatement, Mockito.times(1)).close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void deleteTest() {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager.getConnection(DB_URL, USER, PASSWORD)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(REQUEST_DELETE)).thenReturn(preparedStatement);

            cut.delete(personTest);

            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(1, personTest.id);
            Mockito.verify(preparedStatement, Mockito.times(1)).executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}