package com.services.dbServices.impl;

import com.models.Person;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import java.sql.*;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class PostgresqlDBServiceTest {

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/p2exampl";
    private static final String USER = "serhiip2";
    private static final String PASSWORD = "99887766";
    private static final String TABLE_NAME = "persons";
    private static final String REQUEST_CREATE = "INSERT INTO " + TABLE_NAME + " (id, fname, lname, age, city) VALUES (?, ?, ?, ?, ?)";
    private static final String REQUEST_READ = "SELECT * FROM " + TABLE_NAME + "\nORDER BY id ASC";
    private static final String REQUEST_UPDATE = "UPDATE " + TABLE_NAME + " SET fname = ?, lname = ?, age = ?, city = ? WHERE id = ?";
    private static final String REQUEST_DELETE = "DELETE FROM " + TABLE_NAME + " WHERE id = ?";

    private final Connection connection = Mockito.mock(Connection.class);
    private final PreparedStatement preparedStatement = Mockito.mock(PreparedStatement.class);
    private final ResultSet resultSet = Mockito.mock(ResultSet.class);
    private final Person personTest = new Person(123, "F1", "L1", 20, "C1");

    private final PostgresqlDBService cut = new PostgresqlDBService();


    @Test
    void createTest() {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager.getConnection(DB_URL, USER, PASSWORD)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(REQUEST_CREATE)).thenReturn(preparedStatement);

            cut.create(personTest);

            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(1, personTest.id);
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(2, personTest.fname);
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(3, personTest.lname);
            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(4, personTest.age);
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(5, personTest.city);
            Mockito.verify(preparedStatement, Mockito.times(1)).executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void createNullPerson() {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager.getConnection(DB_URL, USER, PASSWORD)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(REQUEST_CREATE)).thenReturn(preparedStatement);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertDoesNotThrow(() -> cut.create(null));
        Mockito.verifyNoInteractions(preparedStatement);
    }

    @Test
    void createExceptionTest() {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager.getConnection(DB_URL, USER, PASSWORD)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(REQUEST_CREATE)).thenReturn(preparedStatement);
            Mockito.doThrow(SQLException.class).when(preparedStatement).setString(2, personTest.fname);

            cut.create(personTest);

            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(1, personTest.id);
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(2, personTest.fname);
            Mockito.verify(preparedStatement, Mockito.times(0)).setString(3, personTest.lname);
            Mockito.verify(preparedStatement, Mockito.times(0)).setInt(4, personTest.age);
            Mockito.verify(preparedStatement, Mockito.times(0)).setString(5, personTest.city);
            Mockito.verify(preparedStatement, Mockito.times(0)).executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static List<Arguments> readTestArgs() {
        return List.of(
                Arguments.arguments(true, 1, 2, 1, 1, 1, 1, 1),
                Arguments.arguments(false, 1, 1, 0, 0, 0, 0, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("readTestArgs")
    void readTest(boolean next, int executeQueryTimes, int nextTimes, int idTimes, int fNameTimes, int lNameTimes,
                  int ageTimes, int cityTimes) {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager.getConnection(DB_URL, USER, PASSWORD)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(REQUEST_READ)).thenReturn(preparedStatement);
            Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
            Mockito.when(resultSet.next()).thenReturn(next).thenReturn(false);

            cut.read();

            Mockito.verify(preparedStatement, Mockito.times(executeQueryTimes)).executeQuery();
            Mockito.verify(resultSet, Mockito.times(nextTimes)).next();
            Mockito.verify(resultSet, Mockito.times(idTimes)).getInt("id");
            Mockito.verify(resultSet, Mockito.times(fNameTimes)).getString("fname");
            Mockito.verify(resultSet, Mockito.times(lNameTimes)).getString("lname");
            Mockito.verify(resultSet, Mockito.times(ageTimes)).getInt("age");
            Mockito.verify(resultSet, Mockito.times(cityTimes)).getString("city");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void updateTest() {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager.getConnection(DB_URL, USER, PASSWORD)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(REQUEST_UPDATE)).thenReturn(preparedStatement);

            cut.update(personTest);

            Mockito.verify(preparedStatement, Mockito.times(1)).setString(1, personTest.fname);
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(2, personTest.lname);
            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(3, personTest.age);
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(4, personTest.city);
            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(5, personTest.id);
            Mockito.verify(preparedStatement, Mockito.times(1)).executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void updateNullPerson() {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager.getConnection(DB_URL, USER, PASSWORD)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(REQUEST_UPDATE)).thenReturn(preparedStatement);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertDoesNotThrow(() -> cut.update(null));
        Mockito.verifyNoInteractions(preparedStatement);
    }

    @Test
    void deleteTest() {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager.getConnection(DB_URL, USER, PASSWORD)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(REQUEST_DELETE)).thenReturn(preparedStatement);

            cut.delete(personTest);

            Mockito.verify(preparedStatement, Mockito.times(1)).setInt(1, personTest.id);
            Mockito.verify(preparedStatement, Mockito.times(1)).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void deleteNullPerson() {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            mockDriveManager.when(() -> DriverManager.getConnection(DB_URL, USER, PASSWORD)).thenReturn(connection);
            Mockito.when(connection.prepareStatement(REQUEST_DELETE)).thenReturn(preparedStatement);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertDoesNotThrow(() -> cut.delete(null));
        Mockito.verifyNoInteractions(preparedStatement);
    }
}