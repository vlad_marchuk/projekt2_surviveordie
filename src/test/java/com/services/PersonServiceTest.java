package com.services;

import com.models.Person;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class PersonServiceTest {

    private final List<Person> personCache = List.of(new Person(2, "a", "b", 10, "c"));
    private final PersonService cut = new PersonService(personCache);


    static List<Arguments> createPersonNullTestArgs() {
        return List.of(
                Arguments.arguments(-1 , "f", "l", 10, "c"),
                Arguments.arguments(2 , "f", "l", 10, "c"),
                Arguments.arguments(1 , "", "l", 10, "c"),
                Arguments.arguments(1 , "  ", "l", 10, "c"),
                Arguments.arguments(1 , null, "l", 10, "c"),
                Arguments.arguments(1 , "1", "l", 10, "c"),
                Arguments.arguments(1 , "-", "l", 10, "c"),
                Arguments.arguments(1 , "f1", "l", 10, "c"),
                Arguments.arguments(1 , " f1", "l", 10, "c"),
                Arguments.arguments(1 , "f s", "l", 10, "c"),
                Arguments.arguments(1 , "f", "l", 0, "c"),
                Arguments.arguments(1 , "f", "l", -1, "c"),
                Arguments.arguments(1 , "f", "l", 121, "c")
        );
    }

    @ParameterizedTest
    @MethodSource("createPersonNullTestArgs")
    void createPersonNullTest(int id, String fName, String lName, int age, String city) {
        Person actual = cut.createPerson(id, fName, lName, age, city);

        assertNull(actual);
    }

    @Test
    void createPerson() {
        Person expected = new Person(1, "AAA", "BBB", 25, "CCC");

        Person actual = cut.createPerson(1, "AAA", "BBB", 25, "CCC");

        assertEquals(expected, actual);
    }
}