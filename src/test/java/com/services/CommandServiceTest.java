package com.services;

import com.factories.DBServicesFactory;
import com.models.Person;
import com.services.dbServices.IDBService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class CommandServiceTest {
    private static final Person person = Mockito.mock(Person.class);
    private final List<Person> personCache = Mockito.mock(List.class);
    private final IDBService currentDBService = Mockito.mock(IDBService.class);
    private final PersonService personService = Mockito.mock(PersonService.class);
    private final DBServicesFactory dbServicesFactory = Mockito.mock(DBServicesFactory.class);
    private final CommandService cut = new CommandService(personCache, personService, dbServicesFactory);

    @BeforeEach
    void init() throws NoSuchFieldException, IllegalAccessException {
        Field field = cut.getClass().getDeclaredField("currentDBService");
        field.setAccessible(true);
        field.set(cut, currentDBService);
    }

    @Test
    void setCurrentDBServiceTest() {
        int id = 1;

        assertDoesNotThrow(() -> cut.setCurrentDBService(id));

        Mockito.verify(dbServicesFactory, Mockito.times(1)).getDBService(id);
    }

    @Test
    void createTest() {

        cut.create(person);

        Mockito.verify(currentDBService, Mockito.times(1)).create(person);
        Mockito.verify(personService, Mockito.times(1)).addPerson(person);
    }

    static Arguments[] readTestArgs() {
        return new Arguments[]{
            Arguments.arguments(1, List.of(person)),
            Arguments.arguments(0, null),
            Arguments.arguments(0, new ArrayList<Person>())
        };
    }

    @ParameterizedTest
    @MethodSource("readTestArgs")
    void readTest(int addAllTimes, List<Person> buff) {
        Mockito.when(currentDBService.read()).thenReturn(buff);

        cut.read();

        Mockito.verify(personCache, Mockito.times(1)).clear();
        Mockito.verify(personCache, Mockito.times(addAllTimes)).addAll(buff);
    }

    @Test
    void updateTest() {

        cut.update(person);

        Mockito.verify(currentDBService, Mockito.times(1)).update(person);
    }

    @Test
    void deleteTest() {

        cut.delete(person);

        Mockito.verify(currentDBService, Mockito.times(1)).delete(person);
        Mockito.verify(personService, Mockito.times(1)).deletePerson(person);
    }
}