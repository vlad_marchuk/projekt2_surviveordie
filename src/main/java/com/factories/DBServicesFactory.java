package com.factories;

import com.services.dbServices.IDBService;

public class DBServicesFactory {

    private IDBService[] services;

    public DBServicesFactory(IDBService[] services) {
        this.services = services;
    }

    public IDBService getDBService(int id) {
        return services[id];
    }
}
