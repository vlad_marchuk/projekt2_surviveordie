package com;

import com.UI.StartUI;
import com.factories.DBServicesFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.models.Person;
import com.services.CommandService;
import com.services.PersonService;
import com.services.dbServices.IDBService;
import com.services.dbServices.impl.*;
import de.undercouch.bson4jackson.BsonFactory;
import redis.clients.jedis.Jedis;
import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    private static final String PATCH_TO_FILES = "src/main/java/com/files/";

    public static void main(String[] args) {
        File jFile = createFile("JsonFileDB.json");
        File bFile = createFile("BinaryFileDB.binary");
        File xFile = createFile("XmlFileDB.xml");
        File yFile = createFile("YamlFileDB.yaml");
        File cFile = createFile("CsvFileDB.csv");
        ObjectMapper jObjectMapper = new ObjectMapper();
        ObjectMapper bObjectMapper = new ObjectMapper(new BsonFactory());
        XmlMapper xObjectMapper = new XmlMapper();
        ObjectMapper yObjectMapper = new ObjectMapper(new YAMLFactory());
        JsonDBService json = new JsonDBService(jFile, jObjectMapper);
        BinaryDBService binary = new BinaryDBService(bFile, bObjectMapper);
        XmlDBService xml = new XmlDBService(xFile, xObjectMapper);
        YamlDBService yaml = new YamlDBService(yFile, yObjectMapper);
        CsvDBService csv = new CsvDBService(cFile);
        PostgresqlDBService postgresql = new PostgresqlDBService();
        MySqlDBService mySql = new MySqlDBService();
        Jedis jedis = new Jedis("localhost", 6379);
        RedisDBService redis = new RedisDBService(jedis, jObjectMapper);
        MongoDBService mongo = new MongoDBService();
        H2DBService h2 = new H2DBService();
//        CassandraDBService cassandra = new CassandraDBService();
        GraphDBService graph = new GraphDBService();

        IDBService[] services = {json, binary, xml, yaml, csv, postgresql, mySql, redis, mongo, h2, graph};

        List<Person> personCache = new ArrayList<>();
        PersonService personService = new PersonService(personCache);
        DBServicesFactory dbServicesFactory = new DBServicesFactory(services);
        CommandService commandService = new CommandService(personCache, personService, dbServicesFactory);

        StartUI app = new StartUI(commandService, personCache, personService);
        app.start();
        app.setVisible(true);
        app.setResizable(false);
        app.setBounds(500, 150, 800, 500);
        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    private static File createFile(String name) {
        File file = new File(PATCH_TO_FILES + name);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("Problem with creating file  ");
                e.printStackTrace();
            }
        }
        return file;
    }
}
