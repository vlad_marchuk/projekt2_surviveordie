package com.services.dbServices.impl;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.models.Person;
import com.services.dbServices.IDBService;
import java.util.ArrayList;
import java.util.List;

public class CassandraDBService implements IDBService {

    private static final String TABLE_NAME = "persons";
    private static final String NODE = "127.0.0.1";
    private Cluster cluster;
    private Session session;

    public CassandraDBService() {
        getConnectionToCassandra(NODE, null);
        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ");
        sb.append(TABLE_NAME);
        sb.append("(");
        sb.append("id int PRIMARY KEY, ");
        sb.append("fname text,");
        sb.append("lname text,");
        sb.append("age int,");
        sb.append("city text);");

        final String query = sb.toString();
        session.execute(query);
        close();
    }

    @Override
    public void create(Person p) {
        getConnectionToCassandra(NODE, null);
        createPerson(p);
        close();
    }

    @Override
    public List<com.models.Person> read() {
        getConnectionToCassandra(NODE, null);
        return getAllPersonsFromCassandra();
    }

    @Override
    public void update(Person p) {
        if (p == null) {
            return;
        }
        getConnectionToCassandra(NODE, null);
        updatePerson(p);
        close();
    }

    @Override
    public void delete(Person p) {
        if (p == null) {
            return;
        }
        getConnectionToCassandra(NODE, null);
        deletePerson(p);
        close();
    }

    public void getConnectionToCassandra(String node, Integer port) {
        Cluster.Builder b = Cluster.builder().addContactPoint(node);
        if (port != null) {
            b.withPort(port);
        }
        cluster = b.build();
        session = cluster.connect();
        createKeyspace("library", "SimpleStrategy", 1);
        useKeyspace("library");
    }


    public void close() {
        session.close();
        cluster.close();
    }


    public void createKeyspace(String keyspaceName, String replicationStrategy, int numberOfReplicas) {
        StringBuilder sb = new StringBuilder("CREATE KEYSPACE IF NOT EXISTS ").append(keyspaceName)
                .append(" WITH replication = {").append("'class':'").append(replicationStrategy)
                .append("','replication_factor':").append(numberOfReplicas).append("};");
        final String query = sb.toString();
        session.execute(query);
    }

    public void useKeyspace(String keyspace) {
        session.execute("USE " + keyspace);
    }

    private void createPerson(Person p) {
        StringBuilder sb = new StringBuilder("INSERT INTO ").append(TABLE_NAME).append("(id, fname, lname, age, city) ")
                .append("VALUES (?,?,?,?,?)");
        final String query = sb.toString();
        com.datastax.driver.core.PreparedStatement preparedStatement = session.prepare(query);
        BoundStatement boundStatement = preparedStatement.bind(p.getId(), p.getFname(), p.getLname(), p.getAge(), p.getCity());
        session.execute(boundStatement);
    }

    public List<Person> getAllPersonsFromCassandra() {
        StringBuilder sb = new StringBuilder("SELECT * FROM ").append(TABLE_NAME);
        final String query = sb.toString();
        com.datastax.driver.core.ResultSet rs =  session.execute(query);
        close();

        List<Person> people = new ArrayList<>();
        for (Row r : rs) {
            Person persons = new Person(r.getInt("id"), r.getString("fname"), r.getString("lname"), r.getInt("age"),
                    r.getString("city"));
            people.add(persons);
        }
        return people;
    }

    private void updatePerson(Person p) {
        StringBuilder sb = new StringBuilder("UPDATE ").append(TABLE_NAME).append(" SET ").append("fname = ?, ")
                .append("lname = ?, ").append("age = ?, ").append("city = ?").append("WHERE ").append("id = ?");
        final String query = sb.toString();
        com.datastax.driver.core.PreparedStatement preparedStatement = session.prepare(query);
        BoundStatement boundStatement = preparedStatement.bind(p.getFname(), p.getLname(), p.getAge(), p.getCity(), p.getId());
        session.execute(boundStatement);
    }

    private void deletePerson(Person p) {
        StringBuilder sb = new StringBuilder("DELETE FROM ").append(TABLE_NAME).append(" WHERE id = ?").append(";");
        final String query = sb.toString();
        com.datastax.driver.core.PreparedStatement preparedStatement = session.prepare(query);
        BoundStatement boundStatement = preparedStatement.bind(p.getId());
        session.execute(boundStatement);

    }

}
