package com.services.dbServices.impl;

import com.models.Person;
import com.services.dbServices.IDBService;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GraphDBService implements IDBService {

    @Override
    public void create(Person person) {

        try (Connection connection = getConnect()) {
            String query = "CREATE (person:Person {id: ?, fname: ?, lname: ?, age: ?, city: ?}) RETURN person";
            PreparedStatement ps = connection.prepareStatement(query);

            ps.setInt(1, person.getId());
            ps.setString(2, person.getFname());
            ps.setString(3, person.getLname());
            ps.setInt(4, person.getAge());
            ps.setString(5, person.getCity());
            ps.executeUpdate();
            ps.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<Person> read() {
        List<Person> rez = new ArrayList<>();
        Person buffer;

        try (Connection connection = getConnect(); Statement statement = connection.createStatement();) {
            String query = "MATCH (person:Person) RETURN person.id, person.fname, person.lname, person.age, person.city";
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                buffer = new Person(result.getInt("person.id"),
                        result.getString("person.fname"),
                        result.getString("person.lname"),
                        result.getInt("person.age"),
                        result.getString("person.city"));
                rez.add(buffer);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return rez;
    }

    @Override
    public void update(Person person) {
        try (Connection connection = getConnect()) {
            String query = "MATCH (person:Person) WHERE person.id=? SET person.fname=? SET person.lname=? " +
                    "SET person.age=? SET person.city=? RETURN person";

            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setInt(1, person.getId());
            preparedStatement.setString(2, person.getFname());
            preparedStatement.setString(3, person.getLname());
            preparedStatement.setInt(4, person.getAge());
            preparedStatement.setString(5, person.getCity());
            preparedStatement.executeUpdate();
            preparedStatement.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void delete(Person p) {
        try (Connection connection = getConnect()) {
            String query = "MATCH (person:Person) WHERE person.id=? DELETE person";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, p.getId());
            preparedStatement.executeQuery();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public Connection getConnect() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(
                    "jdbc:neo4j:bolt://localhost:7687", "neo4j", "123qwe");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}