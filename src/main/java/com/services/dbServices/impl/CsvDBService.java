package com.services.dbServices.impl;

import com.models.Person;
import com.opencsv.CSVWriter;
import com.opencsv.bean.*;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.services.dbServices.IDBService;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CsvDBService implements IDBService {

    private File file;

    public CsvDBService(File file) {
        this.file = file;
    }

    @Override
    public void create(Person p) {
        if (p == null) {
            return;
        }
        ArrayList<Person> buff;
        List<Person> l = readFromDb();
        if (l == null) {
            buff = new ArrayList<>();
        } else {
            buff = new ArrayList<>(l);
        }
        buff.add(p);
        writeToDB(buff);
    }

    @Override
    public List<Person> read() {
        return readFromDb();
    }

    @Override
    public void update(Person p) {
        if (p == null) {
            return;
        }
        ArrayList<Person> buff;
        List<Person> l = readFromDb();
        if (l == null) {
            buff = new ArrayList<>();
        } else {
            buff = new ArrayList<>(l);
        }
        for (int i = 0; i < buff.size(); i++) {
            if (buff.get(i).id == p.id) {
                buff.set(i, p);
                break;
            }
        }
        writeToDB(buff);
    }

    @Override
    public void delete(Person p) {
        if (p == null) {
            return;
        }
        ArrayList<Person> buff;
        List<Person> l = readFromDb();
        if (l == null) {
            buff = new ArrayList<>();
        } else {
            buff = new ArrayList<>(l);
        }
        for (int i = 0; i < buff.size(); i++) {
            if (buff.get(i).id == p.id) {
                buff.remove(i);
                break;
            }
        }
        writeToDB(buff);
    }

    private void writeToDB(List<Person> persons) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {

            StatefulBeanToCsv<Person> beanToCsv = new StatefulBeanToCsvBuilder<Person>(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .build();

            beanToCsv.write(persons);
        } catch (CsvDataTypeMismatchException | CsvRequiredFieldEmptyException |
                IOException e) {
            System.out.println("Problem with write in csv ");
            e.printStackTrace();
        }
    }


    private List<Person> readFromDb() {
        if (file.length() == 0) {
            return null;
        }
        List<Person> readedList = null;

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            HeaderColumnNameMappingStrategy<Person> strategy
                    = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(Person.class);

            CsvToBean<Person> csvToBean = new CsvToBeanBuilder<Person>(br)
                    .withMappingStrategy(strategy)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            readedList = csvToBean.parse();
        } catch (IOException e) {
            System.out.println("Problem with read in csv ");
            e.printStackTrace();
        }

        return readedList;
    }
}
