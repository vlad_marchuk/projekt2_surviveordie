package com.services.dbServices.impl;

import com.exceptions.FailedConnectionException;
import com.models.Person;
import com.services.dbServices.IDBService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySqlDBService implements IDBService {

    private static final String TABLE_NAME = "persons";
    private static final String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
    private static final String CONNECTION_STRING = "jdbc:mysql://localhost:3306/vladSQLdb";
    private static final String PASSWORD = "123qwe";
    private static final String LOGIN = "vladSQL";

    public MySqlDBService() {
        createTable();
    }

    @Override
    public void create(Person p) {
        if (p == null) {
            return;
        }

        try (
                Connection connection = getConnectionToMysql();
                PreparedStatement ps = connection.prepareStatement(createPerson())
        ) {
            ps.setInt(1, p.id);
            ps.setString(2, p.fname);
            ps.setString(3, p.lname);
            ps.setInt(4, p.age);
            ps.setString(5, p.city);
            ps.executeUpdate();
        } catch (SQLException | FailedConnectionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Person> read() {

        List<Person> rez = new ArrayList<>();
        Person buffer;
        try (
                Connection connection = getConnectionToMysql();
                PreparedStatement ps = connection.prepareStatement(getAllPersonsFromMysql())
        ) {
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {

                buffer = new Person(resultSet.getInt("id"), resultSet.getString("fname"),
                        resultSet.getString("lname"), resultSet.getInt("age"),
                        resultSet.getString("city"));
                rez.add(buffer);
            }
            return rez;
        } catch (SQLException | FailedConnectionException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Person p) {
        if (p == null) {
            return;
        }

        try (
                Connection connection = getConnectionToMysql();
                PreparedStatement ps = connection.prepareStatement(updatePerson())
        ) {
            ps.setString(1, p.fname);
            ps.setString(2, p.lname);
            ps.setInt(3, p.age);
            ps.setString(4, p.city);
            ps.setInt(5, p.id);
            ps.executeUpdate();
        } catch (SQLException | FailedConnectionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Person p) {
        if (p == null) {
            return;
        }

        try (
                Connection connection = getConnectionToMysql();
                PreparedStatement ps = connection.prepareStatement(deletePerson());
        ) {
            ps.setInt(1, p.id);
            ps.execute();
        } catch (SQLException | FailedConnectionException e) {
            e.printStackTrace();
        }
    }

    private Connection getConnectionToMysql() throws FailedConnectionException {
        try {
            Class.forName(DRIVER_NAME);
            return DriverManager.getConnection(CONNECTION_STRING, LOGIN, PASSWORD);
        } catch (SQLException | ClassNotFoundException e) {
            throw new FailedConnectionException(e.getMessage());
        }
    }

    private void createTable() {
        try (
                Connection connection = getConnectionToMysql();
                PreparedStatement ps = connection.prepareStatement(createTab())
        ) {
            ps.executeUpdate();
        } catch (SQLException | FailedConnectionException e) {
            e.printStackTrace();
        }
    }

    private String createTab() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(id int4 NOT NULL PRIMARY KEY, fname VARCHAR(60) NOT NULL," +
                " lname VARCHAR(60) NOT NULL, age int4 NOT NULL, city VARCHAR(60) NOT NULL);";
    }

    private String createPerson() {
        return "INSERT INTO " + TABLE_NAME + " (id, fname, lname, age, city) VALUES (?, ?, ?, ?, ?)";
    }

    private String getAllPersonsFromMysql() {
        return "SELECT * FROM " + TABLE_NAME + "\n" +
                "ORDER BY id ASC";
    }

    private String updatePerson() {
        return "UPDATE " + TABLE_NAME + " SET fname = ?, lname = ?, age = ?, city = ? WHERE id = ?";
    }

    private String deletePerson() {
        return "DELETE FROM " + TABLE_NAME + " WHERE id = ?";
    }
}



