package com.services.dbServices.impl;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.models.Person;
import com.services.dbServices.IDBService;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XmlDBService implements IDBService {

    private File file;
    private XmlMapper mapper;

    public XmlDBService(File file, XmlMapper mapper) {
        this.file = file;
        this.mapper = mapper;
    }

    @Override
    public void create(Person p) {
        if (p == null) {
            return;
        }
        ArrayList<Person> buff;
        List<Person> l = readFromDb();
        if (l == null) {
            buff = new ArrayList<>();
        } else {
            buff = new ArrayList<>(l);
        }
        buff.add(p);
        writeToDB(buff);
    }

    @Override
    public List<Person> read() {
        return readFromDb();
    }

    @Override
    public void update(Person p) {
        if (p == null) {
            return;
        }
        ArrayList<Person> buff;
        List<Person> l = readFromDb();
        if (l == null) {
            buff = new ArrayList<>();
        } else {
            buff = new ArrayList<>(l);
        }
        for (int i = 0; i < buff.size(); i++) {
            if (buff.get(i).id == p.id) {
                buff.set(i, p);
                break;
            }
        }
        writeToDB(buff);
    }

    @Override
    public void delete(Person p) {
        if (p == null) {
            return;
        }
        ArrayList<Person> buff;
        List<Person> l = readFromDb();
        if (l == null) {
            buff = new ArrayList<>();
        } else {
            buff = new ArrayList<>(l);
        }
        for (int i = 0; i < buff.size(); i++) {
            if (buff.get(i).id == p.id) {
                buff.remove(i);
                break;
            }
        }
        writeToDB(buff);
    }

    private void writeToDB(List<Person> persons) {
        try {
            //сериализация: 1-куда, 2-что
            mapper.writeValue(file, persons);
        } catch (IOException e) {
            System.out.println("Problem with write in xml ");
            e.printStackTrace();
        }
    }

    private List<Person> readFromDb() {
        if (file.length() == 0) {
            return null;
        }
        List<Person> readedList =  null;
        try {
            //десериализация
            readedList = List.of(mapper.readValue(file, Person[].class));
        } catch (IOException e) {
            System.out.println("Problem with read in xml ");
            e.printStackTrace();
        }
        return readedList;
    }
}
