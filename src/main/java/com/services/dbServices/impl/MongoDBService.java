package com.services.dbServices.impl;

import com.models.Person;
import com.mongodb.client.*;
import com.mongodb.client.model.Updates;
import com.services.dbServices.IDBService;
import org.bson.Document;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static com.mongodb.client.model.Filters.eq;

public class MongoDBService implements IDBService {

    private static final String DB_URL = "mongodb://127.0.0.1:27017";
    private static final String DB_NAME = "vlad-mongo-databasse";
    private static final String COLLECTION_NAME = "user";


    @Override
    public void create(Person p) {
        try (MongoClient mongoClient = MongoClients.create(DB_URL)) {
            MongoDatabase database = mongoClient.getDatabase(DB_NAME);
            MongoCollection<Document> personsMongoCollection = database.getCollection(COLLECTION_NAME);
            Document document = new Document(Map.of("id", p.getId(), "fname", p.getFname(), "lname", p.getLname(), "age", p.getAge(), "city", p.getCity()));
            personsMongoCollection.insertOne(document);
        }
    }

    @Override
    public List<Person> read() {
        ArrayList<Person> bufferPerson = new ArrayList<>();

        try (MongoClient mongoClient = MongoClients.create(DB_URL)) {
            MongoDatabase database = mongoClient.getDatabase(DB_NAME);
            MongoCollection<Document> personsMongoCollection = database.getCollection(COLLECTION_NAME);
            FindIterable<Document> iterDoc = personsMongoCollection.find();

            for (Document readDocument : iterDoc) {
                Person readPerson = new Person();
                readPerson.setId((int) Double.parseDouble(String.valueOf(readDocument.get("id"))));
                readPerson.setFname(readDocument.getString("fname"));
                readPerson.setLname(readDocument.getString("lname"));
                readPerson.setAge((int) Double.parseDouble(String.valueOf(readDocument.get("age"))));
                readPerson.setCity(readDocument.getString("city"));
                bufferPerson.add(readPerson);
            }
            return bufferPerson;
        }
    }

    @Override
    public void update(Person p) {
        try (MongoClient mongoClient = MongoClients.create(DB_URL)) {
            MongoDatabase database = mongoClient.getDatabase(DB_NAME);
            MongoCollection<Document> personsMongoCollection = database.getCollection(COLLECTION_NAME);

            personsMongoCollection.updateOne(eq("id", p.getId()), Updates.set("fname", p.getFname()));
            personsMongoCollection.updateOne(eq("id", p.getId()), Updates.set("lname", p.getLname()));
            personsMongoCollection.updateOne(eq("id", p.getId()), Updates.set("age", p.getAge()));
            personsMongoCollection.updateOne(eq("id", p.getId()), Updates.set("city", p.getCity()));
        }

    }

    @Override
    public void delete(Person p) {
        try (MongoClient mongoClient = MongoClients.create(DB_URL)) {
            MongoDatabase database = mongoClient.getDatabase(DB_NAME);
            MongoCollection<Document> personsMongoCollection = database.getCollection(COLLECTION_NAME);
            personsMongoCollection.deleteOne(eq("id", p.getId()));
        }
    }
}
