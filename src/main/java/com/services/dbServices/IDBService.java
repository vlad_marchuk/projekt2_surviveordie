package com.services.dbServices;

import com.models.Person;
import java.util.List;

public interface IDBService {

    void create(Person p);

    List<Person> read();

    void update(Person p);

    void delete(Person p);
}
