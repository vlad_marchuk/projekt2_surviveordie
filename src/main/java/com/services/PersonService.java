package com.services;

import com.models.Person;
import org.apache.commons.lang3.StringUtils;
import java.util.List;

public class PersonService {

    private List<Person> personCache;

    public PersonService(List<Person> personCache) {
        this.personCache = personCache;
    }

    public Person createPerson(int id, String fName, String lName, int age, String city) {
        if (!validateId(id) || !validateName(fName) || !validateName(lName) || !validateAge(age) || !validateName(city)) {
            return null;
        } else {
            return new Person(id, fName, lName, age, city);
        }
    }

    public boolean deletePerson(Person p) {
        return personCache.remove(p);
    }

    public boolean addPerson(Person p) {
        return personCache.add(p);
    }

    public boolean validateId(int id) {
        if (id < 0) {
            return false;
        }
        if (personCache.isEmpty()) {
            return true;
        }
        for (Person p: personCache) {
            if (p.id == id) {
                return false;
            }
        }
        return true;
    }

    public boolean validateName(String name) {
        if (StringUtils.isBlank(name) || !name.matches("[a-zA-Z]+")) {
            return false;
        }
        return true;
    }

    public boolean validateAge(int age) {
        if (age < 1 || age > 120) {
            return false;
        }
        return true;
    }
}
