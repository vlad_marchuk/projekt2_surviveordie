***Менеджер баз данных*** - позволяет считывать, записывать и обрабатывать список людей из файлов различных форматов

и баз данных. Создан в рамках обучения [DevEducation](https://deveducation.com/en/) - Base Java 2021. Ukraine.

#### Список операций
- select datastore - выбор текущего хранилища
- read - считывание всех персон в выбраном хранилище
- create - создание новой персоны
- update - обновление данных выбраной персоны
- delete - удаление выбраной персоны

#### Создатели
[@Serhii9Sol](https://bitbucket.org/Serhii9Sol/)
[@Vlad_Marchuk](https://bitbucket.org/vlad_marchuk/)
[@Sergii_Bykov](https://bitbucket.org/sergii_bykov/)